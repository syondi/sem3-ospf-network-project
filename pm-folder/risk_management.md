# Risk management


## Excepted Risks

**Risk**: Lack of time and manpower to complete tasks

**Identification**: To identify this risk, the PM should see a connection between main-tasks aswell as sub-tasks are not completed to the due date.

**Assest**: This is a threat in regard of it can accour on many occasions.

**Likelihood and impact:** The likelihood is not well defined because it can't be foreseen easily. The impact of this risk will be quite significant to our part of the project. Our objectives will not be reached, and therefore not make it to the end project date, which cannot be moved. This will not only be a concern for us, but also other groups not being able to connect to us.

**How to respond to the risk:** Longer workhours, ask the superviser for more manpower.

___

**Risk**: Other groups OSPF area problems, so we can't connect.

**Identification**: Advertised or discussed by the group, that they will have problems, having it done on the date.

**Assest**: This is a threat to the project, because not all groups will be able to connect.

**Likelihood and impact:** The likelihood of this will be low, because we have project managers which have done this before. They can step in and help. 
It will not have a huge impact on the project because we can still connect to other groups who has it all working.

**How to respond to the risk:** We can only respond by either helping, if we have the time and resources to help fix the issue. If we can't or must not, then we will just have to wait.