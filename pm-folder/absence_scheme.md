# Absence Scheme ✔

| Name                             | Monday | Tuesday | Wednesday | Thursday | Friday |
| -------------------------------- | ------ | ------- | --------- | -------- | ------ |
| Sebastian Mathias Thomle Mason   | ✔      | ✔       | ✔         | ✔        | ✔        |
| André Filipe Ferreira Dos Santos | ✔      |         | ✔         |          |        |
| Anthony James Peak               | ✔      | ✔       | ✔         | ✔        |        |
| Daniel Mohr Maslygan Lilleballe  | ✔      | ✔       | ✔         | ✔        |        |
| Dziugas Juska                    | ✔      | ✔       | ✔         | ✔        | ✔        |
| Kasper Jensen                    | ✔      | ✔       | ✔         | ✔        | ✔        |
| Kevin van der Wouw               | ✔      | ✔       | ✔         | ✔        | ✔        |
| Pepijn Pinckers                  | ✔      | ✔       | ✔         | ✔        | ✔        |
