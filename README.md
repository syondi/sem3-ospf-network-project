# SEM3-OSPF-Network-Project

This project was designed in the aim of delivering a functional OSPF network through the use of a users laptop - which would then connect to a backbone network and deliver dynamic connectivity between connected devices.

## Project Overview

The aim of the project is to develop the new infrastructure for The Company. In each student group, the environment must be built on top of a Type-2 Hypervisor(Vmworkstation) and the routers used must be based on Junipers Juniper vSRX image. Furthermore, it is a wish from The Company that each location will be connected to an ISP Backbone based on OSPF. This backbone will be built on 2 x Juniper EX4200-48p switches using Virtual Routers (routing-instance virtual-router).



## Project Members

### Project Managers

This project has two managers and they are the following.

| Name                   | Role            | University | Email               |
| ---------------------- | --------------- | ---------- | ------------------- |
| Kyle Mcshane           | Project Manager | UCL        | kyle0055@edu.eal.dk |
| Kasper Jessen          | Project Manager | UCL        | kasp6851@edu.eal.dk |
| Anatoli Plamenov Penev | Project Manager | UCL        | anat0024@edu.eal.dk |


### Project Members

| Name                             | Role   | University | Email                              |
| -------------------------------- | ------ | ---------- | ---------------------------------- |
| Sebastian Mathias Thomle Mason   | Member | UCL        | seba7286@edu.eal.dk                |
| André Filipe Ferreira Dos Santos | Member | UCL        | andr44a3@edu.eal.dk                |
| Anthony James Peak               | Member | UCL        | anth0662@edu.eal.dk                |
| Daniel Mohr Maslygan Lilleballe  | Member | UCL        | dani7378@edu.eal.dk                |
| Dziugas Juska                    | Member | UCL        | dziu0013@edu.eal.dk                |
| Kasper Jensen                    | Member | UCL        | kasp7547@edu.eal.dk                |
| Kevin van der Wouw               | Member | Fontys     | kevin.vanderwouw@student.fontys.nl |
| Pepijn Pinckers                  | Member | Fontys     | p.pinckers@student.fontys.nl       |

# Resources

We have compiled a list of things that you may/will need for this project the following resources...

## Requirements

In order to have the smoothest time using this project we the pms have come up with a list of software that we suggest you guys use to have an easier time in the project - they are the following...

### Git-lab

We will use Gitlab are our project management tool and a way to centralize all our files and data, this is a useful tool because it supports source control and project management in the form of issues/milestones - and obviously you're using Git-lab to read this right now :)

### Riot Account

Riot is a messaging program that we will use through-out the project - if theres a question you have for any of us please use the riot room, create an account and find one of us for an invite to the room.

Our riot handles are the following...

    Kyle Mcshane    -   @syondi
    Kasper Jessen   -   @kasperr

[Riot.im can be downloaded from here.](https://about.riot.im/)

### VMware Workstation 12 or higher

you can use a trial version from the website if you don't already have a license/

[VMware can be found here.](https://www.vmware.com/dk/products/workstation-pro/workstation-pro-evaluation.html)

### Diagram Editing Software

* Microsoft Visio (You can grab this from Microsoft Imagine via your schools e-mail)
* yEd - Is a good alternative that runs natively on linux it's useful but Visio is preferred.

[yEd can be downloaded here.](https://www.yworks.com/products/yed/download)

As a side note you can find some handy juniper stencils from [here](https://www.juniper.net/us/en/products-services/icons-stencils/)

## Additional Software

These two tools are useful in the project as they make working on each file locally so much simpler - so if you do chose to install these please do install both.

### Visual Code

Visual Code is a handy IDE (Editor) that can support multiple languages and filetypes and supports git source control.

We use this tool as our text editor and a way to manage multiple files at once, additionally we manage the repository here and write new documents in markdown format - it may be beneficial for you guys to learn markdown it's a relatively simple way to write documents and issues.

[You can download it here.](https://code.visualstudio.com/download)

### Git

Git is a source control program that allows for synchronous sharing of files between members using a group.

Windows users can download it from here.

[Git for windows users can be downloaded here](https://git-scm.com/)

Alternatively Linux users can use the following command in a terminal to install git...

    sudo apt-get install git
