# Weekly Agenda

Here you will be able to find the current plans for the week.

## Monday

| Plans                     | Notes                                                                                                                | Resources |
| ------------------------- | -------------------------------------------------------------------------------------------------------------------- | --------- |
| Introduction              | A brief introduction to the project and its project managers and how we are going to manage things.                  |           |
| Workstations setup        | Time taken to bring every-ones working environment up to speed and to hand out necessary tools/files.                |           |
| VM-Ware WS Introduction   | VM-Nets/Lan Segments work - how to install virtual machines.                                                         |           |
| Juniper Introduction      | A simple course into the juniper CLI and hierarchy and how to access these guest devices through the hosts terminal. |           |
| Basic JunOS Configuration | Host-Name, New Users, Loopback Address. Management Interface,Router ID,                                                                                                                     |           |

## Tuesday

|           | Plans                                         | Notes | Resources |
| --------- | --------------------------------------------- | ----- | --------- |
| Monday    | Group Introduction, installation of software, |       |           |
| Tuesday   |                                               |       |           |
| Wednesday | HLD                                           |       |           |
| Thursday  |                                               |       |           |
| Friday    |                                               |       |           |   