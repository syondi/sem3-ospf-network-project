# JunOS Router - First Steps

This guide will aim to teach some basic configurations which will come in handy when configurating a router for the first time.

Change123

## First-Time Login

### Login as Root Account

When logging into a JunOS router for the first time you will be greeted with the message.

    Amnesiac (ttya0)

This means that the device is in its factory default state and that it bears no special configurations than the default - in order to login enter the following credentials when prompted.

    login: root

### Entering Configuration Mode

When logging into the router you will be placed into the devices kernel, for our purposes of configuration we need to reach the configuration mode - where we will make device changes.

In order to do this you must first enter operational mode and then enter the configuration mode by the following method.

    root@router% cli

    root@router> edit

    [edit]
    root@router#

Notice how the symbols change at the end of the user prompt, you can tell what mode you are currently in via looking at these symbols, I have created a table below that explains these modes more.

Additionally it is important to note that you will be placed at the top of the hierarchy in edit mode in order to be more specific you can change the level you are at, must this will be covered later.

| Level         | Symbol | Usage                                                              |
| ------------- | ------ | ------------------------------------------------------------------ |
| Kernel        | %      | Default Login Hierarchy                                            |
| Operational   | >      | Device operations such as monitoring and troubleshooting           |
| Configuration | #      | Mode for device configuration of interfaces/protocols/users etc... |

You can find operational mode commands [here](https://www.juniper.net/documentation/en_US/junos/topics/concept/junos-cli-operational-mode-commands-overview.html)
Additionally configuration mode overview can be found [here](https://www.juniper.net/documentation/en_US/junos/topics/topic-map/cli-configuration.html)

### Setting the Root Accounts password

Before any configuration changes may be made to the router the root accounts password MUST be set first - in order to do this you will have to use the following command.

    [edit]
    root@srx12# set system root-authentication plain-text-password
    New Password: *********
    Retype new password: *********

Now to make the changes into the current running configuration you must commit it, you can do this by the following

    [edit]
    root@srx12# commit

it should take a few seconds to complete and validate, then from now on you must enter the password you used in order to login the root account.

### Create a new user

It is typically good practice to set a new user account for your own use instead of having multiple people on the same default root account, in order to add a new account you can do the following to create a new user with the login newuser123

    [edit]
    root@srx12# set system login user newuser123

Now this user will need some sort of authentication for logging in you can use multiple methods such as ssh or a simple password, for our purposes here we will use a standard encrypted password.

    [edit]
    root@srx12# set system login user user123 authentication plain-text-password

You will now be prompted to enter a password and validate it.

We now need to set the users privilege level to that of root by using the following command..

    [edit]
    root@srx12# set system login user user123 class super-user

And finally you can optionally set this user to have a "full-name" as a way of identifying the persons name as opposed to their login name.

    [edit]
    root@srx12# set system login user user123 full-name "kyle mcshane"

As a side note you can enter this user and its credentials all at once as JunOS supports this, by using the following command.

    [edit]
    root@srx12# set system login user user123 full-name "kyle-mcshane" class super-user authentication plain-text-password

once you have created this user don't forget to commit the changes by using 

    [edit]
    root@srx12#commit

## Set system host-name

This will change the devices host-name so you can easily identify the device on the network - this is really useful to use because you can see what device you are on when you have multiple terminals open.

    [edit]
    root@srx12# set system host-name MyNewSrx

Then commit the changes!

    [edit]
    root@srx12# commit
    commit complete
    newuser123@MyNewSrx#

## Loopback Interface (lo0)

The loopback interface is a virtual interface on the device and is always the first active interface that goes live when booting the device, It is normally used so servers and services on the same device can still reach each other - even if the device has no physical network interfaces.

It is important to note that the loopback interface is used for the device to route back on itself so this address should not be advertised to other routers.

On a normal computer this loopback interface is normally given the address

     127.0.0.1

This is classified as a martian-address is not valid for routing - although it is not route-able it is nececcary to set a unique loopback for each host on your network.

You should select an IP Address in the absolute-range meaning a /32 as only one address should be used for the loopback as Junos OS requires that the loopback interface always be configured with a /32 network mask because the Routing Engine is essentially a host..

In order to set the loopback...

    [edit]
    newuser123@MyNewSrx# set interface lo0 unit 0 family inet address 192.0.2.27/32

More information on loopback addresses can be found [here.](https://www.juniper.net/documentation/en_US/junos/topics/task/configuration/loopback-interface.html)