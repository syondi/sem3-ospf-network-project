# Using SSH-keys instead of passwords on Juniper SRX v12.1


This guide will not only teach the use of keys, but also how to create a ssh config file to quick access routers, and proxy_jump thorugh them. (Using the network to jump through the routers using ssh.)

This guide will be done thorugh the terminal, using bash. Therefore if you aren't a linux user, consider downloading [gitbash](https://git-scm.com/downloads).

This is needed to gain bash commands used for ssh connection and creating keys.


## Step procedures.

* **1. Creating a ssh-key and deploying.**

Open a bash terminal (Git-bash for windows). Use the command "cd" and nothing else, to get to your main users directory. This will be where the folder ".ssh" will be created. This folder contains the public and private key, aswell as config file.

To create the folder and key pairs, use the commands below:

---

    ssh-keygen
---
 Hit enter a bunch of times to skip thorugh output.
   **Unless you want to change the directory for .ssh to be saved in**

Now we want to copy the public-key to and deploy it to our router.

The id_rsa is your private key.
The id_rsa.pub is your public key

Use the command below to dispaly the public key contents.

    cat .ssh/id_rsa.pub

output should look something like below:
    
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDUsLw6uxybz3e1A0gYoH/ZQpMKyE8u56D0EPO5WxLE/+BRKeQespifWe8OMCUhhtlN0/f9Fg6HL6PV8O8WpXF5EChYDWGXgQz8txf4ZGEnpEJJQXD+dfIxVPptvbcMHPHGxeFZlRQ2t3s6eBIqq9g/bqJwpIvNJZgPYeDN4X0EGa+PTABTHE5WDIfd1yq242nKRc1XHVw8CbxLCz9JPCZbCF9LPQvVEqIzq5hMY/o5YdLhSEpf/nd+XDaQ5GCkDxpLPUwwJljnndjPzc4t0/pt4Z7HfyG/2HNfEykll1krGY6NwenBfBUW0orbUFGQucXA702ulEMycHGFzDAdde4z bo@ubuntu

*Don't worry you can't use my public for malicious use :)* Nor your own public key. 

<u>**Just don't share your private key, that is the key that handles decryption of public key encryption**</u>

Copy the output of the command **without white spaces!**

---

**Allowing ssh services**

On normal linux/BSD devices you can use the command "ssh-copy-id root@(ip)" to store your key inside a machine, but the vsrx v12.1 is an exception to this.

On the srx we need to input the key inside the CLI under edit mode.

Before this we need to enable ssh service on the router. This is done like below.

    set system services ssh
    commit full

After this we test SSH acces by using the below command in the terminal:

    ssh root@(ip of GW interface)

Enter your password when it promts and gain access.

If this succeded, that means ssh works and intended.

---

**Deploy the key to the SRX on root user.**

So again inside edit mode on the srx, we can deploy our ssh_key.

Using the commands below will deploy the key.

    set system root-authentication ssh-rsa "(public key)"
    commit full

Now the key is deployed there is still an error with the kernel on version 12.1 on the vsrx. (GJ juniper)

Luckily I have a fix for this issue, by editing permissions inside the srxs BSD kernel.

Therefore log into the kernel mode, which is the first mode when logged into root account. The mode has the showing user as "root%"

This is notes for fixing VSRX SSH-KEYS
======================================

* ssh-key Juniper Devices

Make key inside (ssh-keygen) in correct directory

Directory = /etc/ssh

* Changing permission of wheel group

__chown -R root:wheel /cf/root__

Vm image will have problems within the /etc/ssh
This is also the location where the key should be added instaed

Check = log messages

After login attempt on ssh, to see the error name.

Fix ssh_agent.exe on Windows 10.
---------------------------------
	1. run git bash
	2. touch ~/.profile
	3. start ~/.profile to open .profile
	4. add the code below to .profile
	5. run Source ~/.profile 

CODE
====
```
#! /bin/bash  
eval `ssh-agent -s`   
ssh-add ~/.ssh/*_rsa  
```

If this is confusing, come and contact "Bo Mikkelsen" and he can assist.

You can now login with the command below

    ssh root@(ip)

You will now get in without a password :)