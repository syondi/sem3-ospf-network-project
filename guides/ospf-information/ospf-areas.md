# OSPF Areas

OSPF distributes it routes through the AS based on the routers area membership, this means that for a router to be successfully configured within OSPF it needs to have at least one of its interfaces configured within an OSPF area, There exists multiple variations of the OSPF area and as such the application of subnet or area must be taken into consideration when creating the new area – as some will have more practical use that others.

Additionally a fundamental part of OSPF is the backbone area, without it dynamic routing between zones can not occur without proper configuration, the backbone is commonly known as Area 0 and should contain a router from each particular zone in order to reach standard connectivity – as demonstrated below the router OSPF_6 would be able to route packets through its own zone to OSPF_3 then across the backbone to any router residing in another zone.

![ospf-areas](ospf-areas.png)

## Standard Areas

The most frequent and common form of an OSPF zone is the standard zone, these are zones in which routers can receive Type 1 (Router) & Type 2 (Network) link state advertisements – these are messages which are extremely useful and are used to build a map of the shortest possible routes available across the area.

In practice a standard area provides the best connectivity and baseline structure required to provide reliable scalability and network availability – this is due to the fact that all routers within a standard zone have a full knowledge of all possible routes, however there are some scenarios where using standard areas will not be the most feasible choice and as such there are a multitude of area types designed to fit a specific set of requirements.

In order to configure an interface to an OSPF area the following configuration command can be used within the Junos architecture, note that the area number and interface number may be changed to suit a different purpose.

**_The method of setting an interface into a standard OSPF area 0_**

    set protocols ospf area 0 interface ge-0/0/0

## Stub Areas

When all routers have been configured with the stub area sub-type the entire area will become known as a ”Stub area” this is a special type of area within OSPF that has been will not receive any Type 5 (External) LSAs.

In place of this the Area Border Router (ABR) will inject a type 3 LSA into the stub area that will flood throughout the stub area and provide a single default route for external addresses, this means that any router residing in the stub area will no longer receive any external routing updates from OSPF and instead use the singular default route which will reduce the hardware requirements of the routers residing in the stub area by eliminating the need for the routers to maintain all of the external routes, this is particularly useful as you will be able to create an area of routers within OSPF that will be functional and reachable even if they do not meet the required hardware specifications to hold a full routing topology of the entire OSPF AS.

![Stub-Areas](Stub-Area.png)

**_Turning an area into a stub area_**

    set protocols ospf area 6 stub  

It is important to note that all routers in the area must be configured to treat the area as a stub area

## Totally-Stubby-Areas

A Totally Stubby Area (TSA) are very similar to the baseline stub area in which they do not receive any Type 4 or 5 LSA updates however in addition they also do not receive any Type 3 (summary) updates, and as such all routing out of the area will solely rely on the singular default route the ABR injects into the area.
