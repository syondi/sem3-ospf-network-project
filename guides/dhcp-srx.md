# Setup DHCP on vSRX

This guide will teach you how to setup DHCP service on a vSRX router.

**Setup the DHCP pool:**

---

    root@srx-1# set system services dhcp pool 192.168.1.0/24 address-range low 192.168.1.5 high 192.168.1.10

---

This command will setup a DHCP pool on the 192.168.1.0/24 subnet. The address range is from 192.168.1.5 to 192.168.1.10. This means that you will be able to have 5 IPs for 5 devices from 192.168.1.5 to 192.168.1.10. If you want more or less IPs then change the address range. 

**Setup lease time use this command:**

---

    root@srx-1# set system services dhcp pool 192.168.1.0/24 default-lease-time 60 maximum-lease-time 60


---

A lease time is how much time the IP has before expiring and then the device needs to ask for a new IP address from the router. Here I put the lease time to 60 seconds for testing purposes. Now you should be able to see your device requesting a new IP every 60 seconds if you monitor traffic. Normally you would put it to 24 hours or something. Remember the command is in seconds. 

**Setup the DHCP routers default gateway:**

---

    root@srx-1# set system services dhcp router 192.168.1.1 


---

This will tell the router that the interface with the address 192.168.1.1 is the one to hand out DHCP addresses. So for example here you would put the address for the interface connected to a user lan. 

**It should look something like this:**

---

    root@srx-1# show system services dhcp
    router {
        192.168.1.1;
    }
    pool 192.168.1.0/24 {
        address-range low 192.168.1.5 high 192.168.1.10;
        maximum-lease-time 60;
        default-lease-time 60;
    }



---


Now if you logon to your workstation you can set the network adapter to Automatic IPv4 DHCP address. And then it will recieve an address from the range that you created in the vSRX. 
